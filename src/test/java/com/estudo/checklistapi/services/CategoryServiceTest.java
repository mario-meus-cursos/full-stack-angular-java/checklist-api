package com.estudo.checklistapi.services;

import com.estudo.checklistapi.exceptions.CatergoryException;
import com.estudo.checklistapi.exceptions.ResourceNotFoundException;
import com.estudo.checklistapi.mappers.CheckListMapper;
import com.estudo.checklistapi.models.entityes.CategoryEntity;
import com.estudo.checklistapi.models.entityes.CheckListItemEntity;
import com.estudo.checklistapi.models.requests.CategoryRequest;
import com.estudo.checklistapi.models.requests.CategoryUpdateRequest;
import com.estudo.checklistapi.models.responses.CategoryResponse;
import com.estudo.checklistapi.repositories.ICategoryRepository;
import com.estudo.checklistapi.repositories.ICheckListItemRepository;
import org.glassfish.jaxb.runtime.v2.util.CollisionCheckStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {


    private CategoryService categoryService;

    @Mock
    private ICategoryRepository iCategoryRepository;
    @Mock
    private CheckListMapper mapper;
    @Mock
    private ICheckListItemRepository iCheckListItemRepository;

    @BeforeEach
    public void initTest() {
        this.categoryService = new CategoryService(iCategoryRepository, mapper, iCheckListItemRepository);
    }

    @Test
    @DisplayName("Should Create a Category Successfully")
    public void addCategoryCase1() {

        //having
        CategoryRequest categoryRequest = CategoryRequest.builder().name("Personal").build();

        when(iCategoryRepository.save(any(CategoryEntity.class))).thenReturn(new CategoryEntity());
        when(mapper.categoryEntityToCategoryResponse(any(CategoryEntity.class))).thenReturn(new CategoryResponse());

        //when
        CategoryResponse categoryResponse =  this.categoryService.addCategory(categoryRequest);

        //then
        Assertions.assertNotNull(categoryResponse);

        verify(iCategoryRepository, times(1)).save(
                argThat(categoryEntityArg -> categoryEntityArg.getName().equals("Personal")
                        && categoryEntityArg.getGuid() != null)
        );
    }

    @Test
    @DisplayName("Should Thrown An Exception When Category Name IsNull Or Empty")
    public void addCategoryCase2() {

        //having
        CategoryRequest categoryRequestNull = CategoryRequest.builder().name(null).build();

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.categoryService.addCategory(categoryRequestNull));
        assertThat(exception.getMessage(), is("Nome da Categoria não pode ser Vazio ou Nulo."));
    }

    @Test
    @DisplayName("Should Thrown An Exception When To Category Name Is Registered")
    public void addCategoryCase3() {

        //having
        CategoryRequest categoryRequestIsPresent = CategoryRequest.builder().name("Personal").build();
        when(iCategoryRepository.findByName(anyString())).thenReturn(Optional.of(new CategoryEntity()));

        //when
        Assertions.assertNotNull(categoryRequestIsPresent);

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.categoryService.addCategory(categoryRequestIsPresent));
        assertThat(exception.getMessage(), is("Categoria " + categoryRequestIsPresent.getName() + " já existe"));
    }

    @Test
    @DisplayName("Should Update A Category Successfully")
    public void updateCategoryCase1() {

        //having
        String guid = UUID.randomUUID().toString();
        String nameAltered = "Other";

        CategoryEntity categorySave = CategoryEntity.builder()
                .guid(guid)
                .name("Personal")
                .build();

        CategoryUpdateRequest categoryUp = CategoryUpdateRequest.builder()
                .guid(guid)
                .name(nameAltered).build();

        when(iCategoryRepository.save(any(CategoryEntity.class))).thenReturn(new CategoryEntity());
        when(iCategoryRepository.findByGuid(guid)).thenReturn(Optional.of(categorySave));
        when(mapper.categoryEntityToCategoryResponse(any(CategoryEntity.class))).thenReturn(new CategoryResponse());


        //when
        CategoryResponse categoryResponse = this.categoryService.updateCategory(categoryUp);

        Assertions.assertNotNull(categoryResponse);

        //then
        verify(iCategoryRepository, times(1)).save(
                argThat(categoryEntityArg -> categoryEntityArg.getName().equals(nameAltered)
                        && categoryEntityArg.getGuid().equals(guid))
        );
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Update A GUID IsNull Or Empty")
    public void updateCategoryCase2() {

        //having
        CategoryUpdateRequest categoryUpdateRequest = CategoryUpdateRequest.builder().name("Simple Name").build();

        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.categoryService.updateCategory(categoryUpdateRequest));

        assertThat(exception.getMessage(), is("Paramentros invalidos para Atualização da Categoria"));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Update A Name IsNull Or Empty")
    public void updateCategoryCase3() {

        //having
        CategoryUpdateRequest categoryUpdateRequest = CategoryUpdateRequest.builder().guid("Any Value").build();

        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.categoryService.updateCategory(categoryUpdateRequest));
        assertThat(exception.getMessage(), is("Paramentros invalidos para Atualização da Categoria"));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Update And GUID And Name IsNull Or Empty")
    public void updateCategoryCase4() {

        //having

        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.categoryService.updateCategory(new CategoryUpdateRequest()));
        assertThat(exception.getMessage(), is("Paramentros invalidos para Atualização da Categoria"));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Update Category And It Does Not Exist")
    public void updateCategoryCase5() {

        //having
        CategoryUpdateRequest categoryRequest = CategoryUpdateRequest.builder().guid("11111").name("Personal").build();
        when(iCategoryRepository.findByGuid(anyString())).thenReturn(Optional.empty());

        //when
        Assertions.assertNotNull(categoryRequest);

        //then
        Exception exception = Assertions.assertThrows(ResourceNotFoundException.class,
                () -> this.categoryService.updateCategory(categoryRequest));

        assertThat(exception.getMessage(), is("Categoria não encontrada."));
    }

    @Test
    @DisplayName("Should Delete A Category Successfully")
    public void deleteCategoryCase1() {

        //having
        String guid = UUID.randomUUID().toString();
        CategoryEntity categoryDelete = CategoryEntity.builder()
                .guid(guid)
                .name("Simple Name")
                .build();

        when(iCategoryRepository.findByGuid(guid)).thenReturn(Optional.of(categoryDelete));
        //when
        this.categoryService.deleteCategory(guid);

        //then
        verify(iCategoryRepository, times(1)).delete(
                argThat(categoryEntityArg -> categoryEntityArg.getName().equals("Simple Name")
                        && categoryEntityArg.getGuid().equals(guid))
        );

    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Delete Category And It Does Not Exist")
    public void deleteCategoryCase2() {

        //having
        when(iCategoryRepository.findByGuid(anyString())).thenReturn(Optional.empty());
        //when

        //then
        Exception exception = Assertions.assertThrows(ResourceNotFoundException.class,
                () -> this.categoryService.deleteCategory("Any value"));

        assertThat(exception.getMessage(), is("Categoria não encontrada."));
    }


    @Test
    @DisplayName("Should Thrown An Exception When Try To Delete Category At GUID And Name IsNull Or Empty")
    public void deleteCategoryCase3() {

        //having
        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.categoryService.deleteCategory(null));

        assertThat(exception.getMessage(), is("GUID Categoria não pode ser Vazio ou Nulo."));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Delete Category in use by the Checklist")
    public void deleteCategoryCase4() {

        //having
        List<CheckListItemEntity> checkListItemEntities = new ArrayList<>();
        checkListItemEntities.add(CheckListItemEntity.builder().guid("Any Value").build());
        when(iCategoryRepository.findByGuid(anyString())).thenReturn(Optional.of(new CategoryEntity()));
        when(iCheckListItemRepository.findByCategoryGuid(anyString())).thenReturn( checkListItemEntities);

        //when

        //then
        Exception exception = Assertions.assertThrows(CatergoryException.class,
                () -> this.categoryService.deleteCategory("Any Value"));

        assertThat(exception.getMessage(), is("Categoria não pode ser Excluida! Catergoria em uso."));
    }

    @Test
    @DisplayName("Should Return A CategoryEntity Successfully")
    public void getCategoryEntityByGuidCase1() {

        //having
        when(iCategoryRepository.findByGuid("Any Value")).thenReturn(Optional.of(new CategoryEntity()));

        //when
        CategoryEntity categoryReturn = categoryService.getCategoryEntityByGuid("Any Value");

        //then
        Assertions.assertNotNull(categoryReturn);
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Get CategoryEntity")
    public void getCategoryEntityByGuidCase2() {

        //having
        when(this.iCategoryRepository.findByGuid(anyString())).thenReturn(Optional.empty());

        //when

        //then
        Exception exception = Assertions.assertThrows(ResourceNotFoundException.class,
                () -> this.categoryService.getCategoryEntityByGuid("Any value"));

        assertThat(exception.getMessage(), is("Categoria não encontrada."));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Get Category At GUID And Name IsNull Or Empty")
    public void getCategoryEntityByGuidCase3() {

        //having
        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.categoryService.getCategoryEntityByGuid(null));

        assertThat(exception.getMessage(), is("GUID Categoria não pode ser Vazio ou Nulo."));
    }

    @Test
    @DisplayName("Should Return A List CategoryEntities Successfully")
    public void findAllCategoryCase1() {

        //having
        when(iCategoryRepository.findAll()).thenReturn(new CollisionCheckStack<CategoryEntity>());
        when(mapper.lislCategoryEntityToListCategoryResponse(anyList())).thenReturn(new CollisionCheckStack<CategoryResponse>());

        //when
        List<CategoryResponse> categoryResponses = categoryService.findAllCategory();

        //then
        Assertions.assertNotNull(categoryResponses);
    }

}
