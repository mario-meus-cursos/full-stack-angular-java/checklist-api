package com.estudo.checklistapi.services;


import com.estudo.checklistapi.exceptions.ResourceNotFoundException;
import com.estudo.checklistapi.mappers.CheckListMapper;
import com.estudo.checklistapi.models.entityes.CategoryEntity;
import com.estudo.checklistapi.models.entityes.CheckListItemEntity;
import com.estudo.checklistapi.models.requests.CheckListItemRequest;
import com.estudo.checklistapi.models.requests.CheckListItemUpdateRequest;
import com.estudo.checklistapi.models.responses.CheckListItemResponse;
import com.estudo.checklistapi.repositories.ICheckListItemRepository;
import org.glassfish.jaxb.runtime.v2.util.CollisionCheckStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CheckListItemServiceTest {

    private CheckListItemService checkListItemService;

    @Mock
    private ICheckListItemRepository iCheckListItemRepository;
    @Mock
    private CategoryService categoryService;
    @Mock
    private CheckListMapper mapper;

    @BeforeEach
    public void initTest() {

        this.checkListItemService = new CheckListItemService(iCheckListItemRepository, categoryService, mapper);

    }

    @Test
    @DisplayName("Should Create a Checklist Successfully")
    public void addCheckListItemCase1() {

        //having
        CheckListItemRequest itemRequestSave = CheckListItemRequest.builder()
                .description("Any Description")
                .isCompleted(false)
                .deadline(LocalDate.now())
                .categoryGuid("123456")
                .build();

        CategoryEntity category = CategoryEntity.builder()
                .guid("123456")
                .name("Simple name")
                .build();

        when(iCheckListItemRepository.save(any(CheckListItemEntity.class))).thenReturn(new CheckListItemEntity());
        when(mapper.checkListItemEntityToCheckListItemResponse(any(CheckListItemEntity.class)))
                .thenReturn(new CheckListItemResponse());
        doReturn(category).when(categoryService).getCategoryEntityByGuid(anyString());

        //when
        CheckListItemResponse checkListItemResponse = this.checkListItemService.addCheckListItem(itemRequestSave);

        //then
        Assertions.assertNotNull(checkListItemResponse);

        verify(iCheckListItemRepository, times(1)).save(
                argThat(checkListItemArg -> checkListItemArg.getDescription().equals("Any Description")
                        && checkListItemArg.getGuid() != null
                        && checkListItemArg.getCategory().getName().equals("Simple name")
                )

        );
    }

    @Test
    @DisplayName("Should Thrown An Exception When Checklist Description Is Required ")
    public void addCheckListItemCase2() {

        //having
        CheckListItemRequest itemRequestSave = CheckListItemRequest.builder()
                .description("")
                .isCompleted(false)
                .deadline(LocalDate.now())
                .categoryGuid("123456")
                .build();

        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.checkListItemService.addCheckListItem(itemRequestSave));
        assertThat(exception.getMessage(), is("Checklist a descrição do item é obrigatório."));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Checklist IsComplete Is Required And Not Null")
    public void addCheckListItemCase3() {

        //having
        CheckListItemRequest itemRequestSave = CheckListItemRequest.builder()
                .description("Any Description")
                .isCompleted(null)
                .deadline(LocalDate.now())
                .categoryGuid("123456")
                .build();

        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.checkListItemService.addCheckListItem(itemRequestSave));
        assertThat(exception.getMessage(), is("Checklist o Status do item é obrigatório."));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Checklist Deadline Is Required And Not Null")
    public void addCheckListItemCase4() {

        //having
        CheckListItemRequest itemRequestSave = CheckListItemRequest.builder()
                .description("Any Description")
                .isCompleted(false)
                .deadline(null)
                .categoryGuid("123456")
                .build();

        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.checkListItemService.addCheckListItem(itemRequestSave));
        assertThat(exception.getMessage(), is("Checklist o Prazo de entrega do item é obrigatório."));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Checklist GUID Is Required And Not Null")
    public void addCheckListItemCase5() {

        //having
        CheckListItemRequest itemRequestSave = CheckListItemRequest.builder()
                .description("Any Description")
                .isCompleted(false)
                .deadline(LocalDate.now())
                .categoryGuid("")
                .build();

        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.checkListItemService.addCheckListItem(itemRequestSave));
        assertThat(exception.getMessage(), is("Checklist a Categoria do item é obrigatório."));

    }

    @Test
    @DisplayName("Should Update a Checklist Successfully")
    public void updateChecklistItemCase1() {

        //having
        String guid = UUID.randomUUID().toString();
        String nameAltered = "Other";

        CategoryEntity category = CategoryEntity.builder()
                .guid("123456")
                .name("Simple name")
                .build();

        CheckListItemEntity listItemEntityReturn = CheckListItemEntity.builder()
                .guid(guid)
                .description("Any Description")
                .isCompleted(false)
                .category(category)
                .deadline(LocalDate.now())
                .postDate(LocalDate.now())
                .build();

        CheckListItemUpdateRequest itemRequestUp = CheckListItemUpdateRequest.builder()
                .guid(guid)
                .description(nameAltered)
                .isCompleted(true)
                .deadline(LocalDate.now())
                .categoryGuid("123456")
                .build();

        when(iCheckListItemRepository.save(any(CheckListItemEntity.class))).thenReturn(new CheckListItemEntity());
        when(iCheckListItemRepository.findByGuid(guid)).thenReturn(Optional.of(listItemEntityReturn));
        when(mapper.checkListItemEntityToCheckListItemResponse(any(CheckListItemEntity.class))).thenReturn(new CheckListItemResponse());
        doReturn(category).when(categoryService).getCategoryEntityByGuid(anyString());

        //when
        CheckListItemResponse checkListItemResponse = this.checkListItemService.updateChecklistItem(itemRequestUp);

        //then
        Assertions.assertNotNull(checkListItemResponse);

        verify(iCheckListItemRepository, times(1)).save(
                argThat(checkListItemArg -> checkListItemArg.getDescription().equals("Other")
                        && checkListItemArg.getGuid().equals(guid)
                        && checkListItemArg.getIsCompleted() == true
                        && checkListItemArg.getCategory().getName().equals("Simple name")
                )

        );
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Update Checklist And It Does Not Exist")
    public void updateChecklistItemCase2() {

        //having
        String guid = UUID.randomUUID().toString();

        CheckListItemUpdateRequest itemRequestUp = CheckListItemUpdateRequest.builder()
                .guid(guid)
                .description("Any Description")
                .isCompleted(true)
                .deadline(LocalDate.now())
                .categoryGuid("123456")
                .build();

        when(iCheckListItemRepository.findByGuid(guid)).thenReturn(Optional.empty());

        //when

        //then
        Assertions.assertNotNull(itemRequestUp);

        Exception exception = Assertions.assertThrows(ResourceNotFoundException.class,
                () -> this.checkListItemService.updateChecklistItem(itemRequestUp));
        assertThat(exception.getMessage(), is("Checklist não encontrado."));

    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Update Checklist At GUID And Name IsNull Or Empty")
    public void updateChecklistItemCase3() {

        //having
        CheckListItemUpdateRequest itemRequestUp = CheckListItemUpdateRequest.builder()
                .guid(null)
                .description("Any Description")
                .isCompleted(true)
                .deadline(LocalDate.now())
                .categoryGuid("123456")
                .build();
        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.checkListItemService.deleteChecklistItem(null));

        assertThat(exception.getMessage(), is("GUID Checklist não pode ser Vazio ou Nulo."));
    }


    @Test
    @DisplayName("Should Return A List ChecklistEntities Successfully")
    public void findAllCheckListCase1() {

        //having

        when(iCheckListItemRepository.findAll()).thenReturn(new CollisionCheckStack<CheckListItemEntity>());
        when(mapper.listCheckListItemEntityToListCheckListItemResponse(anyList()))
                .thenReturn(new CollisionCheckStack<CheckListItemResponse>());

        //when
        List<CheckListItemResponse> checkListItemResponseList = checkListItemService.findAllCheckList();

        //then
        Assertions.assertNotNull(checkListItemResponseList);

    }

    @Test
    @DisplayName("Should Delete A Checklist Successfully")
    public void deleteCategoryCase1() {

        //having
        String guid = UUID.randomUUID().toString();
        CategoryEntity category = CategoryEntity.builder()
                .guid("123456")
                .name("Simple name")
                .build();

        CheckListItemEntity listItemEntityReturn = CheckListItemEntity.builder()
                .guid(guid)
                .description("Any Description")
                .isCompleted(false)
                .category(category)
                .deadline(LocalDate.now())
                .postDate(LocalDate.now())
                .build();

        when(iCheckListItemRepository.findByGuid(guid)).thenReturn(Optional.of(listItemEntityReturn));
        //when
        this.checkListItemService.deleteChecklistItem(guid);

        //then
        verify(iCheckListItemRepository, times(1)).delete(
                argThat(checkListItemArg -> checkListItemArg.getDescription().equals("Any Description")
                        && checkListItemArg.getGuid().equals(guid)
                        && checkListItemArg.getIsCompleted() == false
                        && checkListItemArg.getCategory().getName().equals("Simple name")
                )
        );

    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Delete Checklist And It Does Not Exist")
    public void deleteCategoryCase2() {

        //having
        String guid = UUID.randomUUID().toString();

        when(iCheckListItemRepository.findByGuid(guid)).thenReturn(Optional.empty());
        //when

        //then
        Exception exception = Assertions.assertThrows(ResourceNotFoundException.class,
                () -> this.checkListItemService.deleteChecklistItem(guid));
        assertThat(exception.getMessage(), is("Checklist não encontrado."));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Delete Checklist At GUID And Name IsNull Or Empty")
    public void deleteCategoryCase3() {

        //having
        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.checkListItemService.deleteChecklistItem(null));

        assertThat(exception.getMessage(), is("GUID Checklist não pode ser Vazio ou Nulo."));
    }

    @Test
    @DisplayName("Should Return A Checklist Successfully")
    public void findChecklistItemByGuidCase1() {

        //having
        String guid = UUID.randomUUID().toString();
        CategoryEntity category = CategoryEntity.builder()
                .guid("123456")
                .name("Simple name")
                .build();

        CheckListItemEntity listItemEntityReturn = CheckListItemEntity.builder()
                .guid(guid)
                .description("Any Description")
                .isCompleted(false)
                .category(category)
                .deadline(LocalDate.now())
                .postDate(LocalDate.now())
                .build();

        when(iCheckListItemRepository.findByGuid(guid)).thenReturn(Optional.of(listItemEntityReturn));
        when(mapper.checkListItemEntityToCheckListItemResponse(any(CheckListItemEntity.class)))
                .thenReturn(new CheckListItemResponse());

        //when
        CheckListItemResponse response = this.checkListItemService.findChecklistItemByGuid(guid);
        //then
        Assertions.assertNotNull(response);
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Get Checklist")
    public void findChecklistItemByGuidCase2() {

        //having
        String guid = UUID.randomUUID().toString();

        when(iCheckListItemRepository.findByGuid(guid)).thenReturn(Optional.empty());

        //when
        //then
        Exception exception = Assertions.assertThrows(ResourceNotFoundException.class,
                () -> this.checkListItemService.findChecklistItemByGuid(guid));

        assertThat(exception.getMessage(), is("Checklist Item não encontrado."));
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Find By Checklist At GUID And Name IsNull Or Empty")
    public void findChecklistItemByGuidCase3() {

        //having
        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.checkListItemService.findChecklistItemByGuid(null));

        assertThat(exception.getMessage(), is("GUID Checklist não pode ser Vazio ou Nulo."));
    }

    @Test
    @DisplayName("Should Update a Checklist status Successfully")
    public void updateIsCompledStatusCase1() {

        //having
        String guid = UUID.randomUUID().toString();

        CategoryEntity category = CategoryEntity.builder()
                .guid("123456")
                .name("Simple name")
                .build();

        CheckListItemEntity listItemEntityReturn = CheckListItemEntity.builder()
                .guid(guid)
                .description("Any Description")
                .isCompleted(false)
                .category(category)
                .deadline(LocalDate.now())
                .postDate(LocalDate.now())
                .build();

        when(iCheckListItemRepository.save(any(CheckListItemEntity.class))).thenReturn(new CheckListItemEntity());
        when(iCheckListItemRepository.findByGuid(guid)).thenReturn(Optional.of(listItemEntityReturn));

        //when
        this.checkListItemService.updateIsCompledStatus(guid, true);

        //then

        verify(iCheckListItemRepository, times(1)).save(
                argThat(checkListItemArg -> checkListItemArg.getDescription().equals("Any Description")
                        && checkListItemArg.getGuid().equals(guid)
                        && checkListItemArg.getIsCompleted() == true
                        && checkListItemArg.getCategory().getName().equals("Simple name")
                )

        );
    }

    @Test
    @DisplayName("Should Thrown An Exception When Try To Update status Checklist At GUID And Name IsNull Or Empty")
    public void updateIsCompledStatusCase2() {

        //having
        //when

        //then
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> this.checkListItemService.updateIsCompledStatus( null, true));

        assertThat(exception.getMessage(), is("GUID Checklist não pode ser Vazio ou Nulo."));
    }

}