package com.estudo.checklistapi.controllers;

import com.estudo.checklistapi.controlles.CheckListItemController;
import com.estudo.checklistapi.models.requests.CheckListItemRequest;
import com.estudo.checklistapi.models.requests.CheckListItemUpdateRequest;
import com.estudo.checklistapi.models.requests.UpdateStatusRequest;
import com.estudo.checklistapi.models.responses.CategoryResponse;
import com.estudo.checklistapi.models.responses.CheckListItemResponse;
import com.estudo.checklistapi.services.CheckListItemService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CheckListItemController.class)
public class CheckListItemControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CheckListItemService itemService;

    @Autowired
    private ObjectMapper objectMapper;

    private final static String URL = "/checklist-items";

    private CheckListItemResponse getChecklistItemResponse(String description, Boolean isCompleted,
                                                           String categoryName, LocalDate deadline) {
        return CheckListItemResponse.builder()
                .guid(UUID.randomUUID().toString())
                .description(description)
                .isCompleted(isCompleted)
                .deadline(deadline)
                .postDate(LocalDate.now())
                .category(CategoryResponse.builder()
                        .guid(UUID.randomUUID().toString())
                        .name(categoryName)
                        .build()
                ).build();
    }

    private CheckListItemRequest getChecklistItemRequest(String description, Boolean isCompleted, LocalDate deadline) {

        return CheckListItemRequest.builder()
                .description(description)
                .isCompleted(isCompleted)
                .deadline(deadline)
                .categoryGuid(UUID.randomUUID().toString())
                .build();
    }

    private CheckListItemUpdateRequest getChecklistItemUpdateRequest(String description, Boolean isCompleted, LocalDate deadline) {

        return CheckListItemUpdateRequest.builder()
                .guid(UUID.randomUUID().toString())
                .description(description)
                .isCompleted(isCompleted)
                .deadline(deadline)
                .categoryGuid(UUID.randomUUID().toString())
                .build();
    }

    @Test
    @DisplayName("Should Call Get All Checklist Items And Return Code 200")
    public void getAllCheckListCase1() throws Exception {

        //having
        List<CheckListItemResponse> findAllData = Arrays.asList(
                getChecklistItemResponse("Item 1", false, "Cat 1", LocalDate.of(2024, 01, 18)),
                getChecklistItemResponse("Item 2", false, "Cat 2", LocalDate.of(2024, 01, 30)),
                getChecklistItemResponse("Item 3", true, "Cat 3", LocalDate.of(2024, 02, 01))
        );
        when(itemService.findAllCheckList()).thenReturn(findAllData);

        //when /then
        this.mockMvc.perform(MockMvcRequestBuilders.get(URL))
               // .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(3)))
                .andExpect(jsonPath("$[0].guid").isNotEmpty())
                .andExpect(jsonPath("$[0].isCompleted").value(false))
                .andExpect(jsonPath("$[0].postDate").isNotEmpty())
                .andExpect(jsonPath("$[0].deadline").value("2024-01-18"))
                .andExpect(jsonPath("$[0].description").value("Item 1"))
                .andExpect(jsonPath("$[0].category").isNotEmpty())
                .andExpect(jsonPath("$[0].category.guid").isNotEmpty())
                .andExpect(jsonPath("$[0].category.name").value("Cat 1"))

                .andExpect(jsonPath("$[1].guid").isNotEmpty())
                .andExpect(jsonPath("$[1].isCompleted").value(false))
                .andExpect(jsonPath("$[1].postDate").isNotEmpty())
                .andExpect(jsonPath("$[1].deadline").value("2024-01-30"))
                .andExpect(jsonPath("$[1].description").value("Item 2"))
                .andExpect(jsonPath("$[1].category").isNotEmpty())
                .andExpect(jsonPath("$[1].category.guid").isNotEmpty())
                .andExpect(jsonPath("$[1].category.name").value("Cat 2"))

                .andExpect(jsonPath("$[2].guid").isNotEmpty())
                .andExpect(jsonPath("$[2].isCompleted").value(true))
                .andExpect(jsonPath("$[2].postDate").isNotEmpty())
                .andExpect(jsonPath("$[2].deadline").value("2024-02-01"))
                .andExpect(jsonPath("$[2].description").value("Item 3"))
                .andExpect(jsonPath("$[2].category").isNotEmpty())
                .andExpect(jsonPath("$[2].category.guid").isNotEmpty())
                .andExpect(jsonPath("$[2].category.name").value("Cat 3"));

    }

    @Test
    @DisplayName("Should Call End Point And Add New Checklist Items And Return Code 201")
    public void createNewChecklistItemCase1() throws Exception {

        //having
        String description = "Item 1";
        Boolean isCompleted = true;
        LocalDate deadline = LocalDate.of(2024, 02, 28);

        when(itemService.addCheckListItem(any(CheckListItemRequest.class)))
                .thenReturn(getChecklistItemResponse(description, isCompleted, "Cat 1", deadline));

        //when /then
        this.mockMvc.perform(MockMvcRequestBuilders.post(URL)
                        .content(objectMapper.writeValueAsString(
                                getChecklistItemRequest(description, isCompleted, deadline)
                        ))
                        .contentType(MediaType.APPLICATION_JSON))
                //.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.guid").isNotEmpty())
                .andExpect(jsonPath("$.isCompleted").value(true))
                .andExpect(jsonPath("$.postDate").isNotEmpty())
                .andExpect(jsonPath("$.deadline").value("2024-02-28"))
                .andExpect(jsonPath("$.description").value("Item 1"))
                .andExpect(jsonPath("$.category").isNotEmpty())
                .andExpect(jsonPath("$.category.guid").isNotEmpty())
                .andExpect(jsonPath("$.category.name").value("Cat 1"));
    }

    @Test
    @DisplayName("Should Call End Point And Update Checklist Item And Return Code 202")
    public void updateChecklistItemCase1() throws Exception {

        //having
        String description = "Item 1";
        Boolean isCompleted = true;
        LocalDate deadline = LocalDate.of(2024, 02, 28);

        when(itemService.updateChecklistItem(any(CheckListItemUpdateRequest.class)))
                .thenReturn(getChecklistItemResponse(description, isCompleted, "Cat 1", deadline));
        //when /then
        this.mockMvc.perform(MockMvcRequestBuilders.put(URL)
                        .content(objectMapper.writeValueAsString(
                                getChecklistItemUpdateRequest(description, isCompleted, deadline)
                        ))
                        .contentType(MediaType.APPLICATION_JSON))
              //  .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.guid").isNotEmpty())
                .andExpect(jsonPath("$.isCompleted").value(true))
                .andExpect(jsonPath("$.postDate").isNotEmpty())
                .andExpect(jsonPath("$.deadline").value("2024-02-28"))
                .andExpect(jsonPath("$.description").value("Item 1"))
                .andExpect(jsonPath("$.category").isNotEmpty())
                .andExpect(jsonPath("$.category.guid").isNotEmpty())
                .andExpect(jsonPath("$.category.name").value("Cat 1"));
    }

    @Test
    @DisplayName("Should Call End Point And Add Delete Checklist Item And Return Code 204")
    public void deleteChecklistItemCase1() throws Exception {

        //having
        String guid = UUID.randomUUID().toString();

        //when /then
        this.mockMvc.perform(MockMvcRequestBuilders.delete(URL.concat("/{guid}"), guid))
               // .andDo(print())
                .andExpect(status().isNoContent());

        verify(itemService, times(1)).deleteChecklistItem(guid);
    }

    @Test
    @DisplayName("Should Call End Point And Patch Status Checklist Item And Return Code 202")
    public void updateCompledStatusCase1() throws Exception {

        //having
        String guid = UUID.randomUUID().toString();

        //when /then
        this.mockMvc.perform(MockMvcRequestBuilders.patch(URL.concat("/{guid}"), guid)
                        .content(objectMapper.writeValueAsString(
                                UpdateStatusRequest.builder().isCompleted(true).build()
                        ))
                        .contentType(MediaType.APPLICATION_JSON))
               // .andDo(print())
                .andExpect(status().isAccepted());

        verify(itemService, times(1)).updateIsCompledStatus(guid, true);
    }

}
