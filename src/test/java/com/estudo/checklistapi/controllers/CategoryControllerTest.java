package com.estudo.checklistapi.controllers;

import com.estudo.checklistapi.controlles.CategoryController;
import com.estudo.checklistapi.models.requests.CategoryRequest;
import com.estudo.checklistapi.models.requests.CategoryUpdateRequest;
import com.estudo.checklistapi.models.responses.CategoryResponse;
import com.estudo.checklistapi.services.CategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CategoryController.class)
public class CategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CategoryService categoryService;

    private final static String URL = "/categories";

    private CategoryResponse getCategoryResponse(String name) {
        return CategoryResponse.builder()
                .guid(UUID.randomUUID().toString())
                .name(name)
                .build();
    }

    @Test
    @DisplayName("Should Call Get All Categories And Return Code 200")
    public void getAllCatergoriesCase1() throws Exception {
        //having
        List<CategoryResponse> findAllData = Arrays.asList(
                getCategoryResponse("Cat 1"),
                getCategoryResponse("Cat 2"),
                getCategoryResponse("Cat 3")
        );

        when(categoryService.findAllCategory()).thenReturn(findAllData);

        //when /then
        this.mockMvc.perform(MockMvcRequestBuilders.get(URL))
             //   .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", hasSize(3)))
                .andExpect(jsonPath("$[0].guid").isNotEmpty())
                .andExpect(jsonPath("$[0].name").value("Cat 1"))

                .andExpect(jsonPath("$[1].guid").isNotEmpty())
                .andExpect(jsonPath("$[1].name").value("Cat 2"))

                .andExpect(jsonPath("$[2].guid").isNotEmpty())
                .andExpect(jsonPath("$[2].name").value("Cat 3"));
    }

    @Test
    @DisplayName("Should Call End Point And Add New Category And Return Code 201")
    public void createNewCategoryCase1() throws Exception {
        //having
        when(categoryService.addCategory(any(CategoryRequest.class))).thenReturn(getCategoryResponse("Any Name"));

        //when /then
        this.mockMvc.perform(MockMvcRequestBuilders.post(URL)
                        .content(objectMapper.writeValueAsString(
                                CategoryRequest.builder().name("Any Name").build()
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
                )
              //  .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.guid").isNotEmpty())
                .andExpect(jsonPath("$.name").value("Any Name"));

    }


    @Test
    @DisplayName("Should Call End Point And Update Category And Return Code 202")
    public void updateCategoryCase1() throws Exception {
        //having
        String guid = UUID.randomUUID().toString();
        CategoryUpdateRequest categoryUp = CategoryUpdateRequest.builder()
                .guid(guid)
                .name("Any Name").build();

        CategoryResponse response = CategoryResponse.builder()
                .guid(guid)
                .name("Other Name").build();

        when(categoryService.updateCategory(any(CategoryUpdateRequest.class))).thenReturn(response);

        //when /then
        this.mockMvc.perform(MockMvcRequestBuilders.put(URL)
                        .content(objectMapper.writeValueAsString(
                                categoryUp
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
                )
             //   .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.guid").isNotEmpty())
                .andExpect(jsonPath("$.name").value("Other Name"));
    }


    @Test
    @DisplayName("Should Call End Point And Delete Category And Return Code 204")
    public void deleteCategoryCase1() throws Exception {
        //having
        String guid = UUID.randomUUID().toString();

        //when /then
        this.mockMvc.perform(MockMvcRequestBuilders.delete(URL.concat("/{guid}"), guid))
             //   .andDo(print())
                .andExpect(status().isNoContent());

        verify(categoryService, times(1)).deleteCategory(guid);

    }


}
