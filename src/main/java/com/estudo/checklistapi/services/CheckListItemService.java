package com.estudo.checklistapi.services;

import com.estudo.checklistapi.exceptions.ResourceNotFoundException;
import com.estudo.checklistapi.mappers.CheckListMapper;
import com.estudo.checklistapi.models.entityes.CheckListItemEntity;
import com.estudo.checklistapi.models.requests.CheckListItemRequest;
import com.estudo.checklistapi.models.requests.CheckListItemUpdateRequest;
import com.estudo.checklistapi.models.responses.CheckListItemResponse;
import com.estudo.checklistapi.repositories.ICheckListItemRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@Slf4j
public class CheckListItemService {

    private ICheckListItemRepository itemRepository;
    private CategoryService categoryService;
    private CheckListMapper mapper;

    //Java 11 não a necessidade da Anotação @Autowired, Pode criar um construtor
    public CheckListItemService(ICheckListItemRepository itemRepository, CategoryService categoryService,
                                CheckListMapper mapper) {
        this.itemRepository = itemRepository;
        this.categoryService = categoryService;
        this.mapper = mapper;
    }

    private void validateCkeckListItem(String description, Boolean isCompleted, LocalDate deadline, String categoryGuid) {
        if (!StringUtils.hasText(description))
            throw new IllegalArgumentException("Checklist a descrição do item é obrigatório.");

        if (isCompleted == null)
            throw new IllegalArgumentException("Checklist o Status do item é obrigatório.");

        if (deadline == null)
            throw new IllegalArgumentException("Checklist o Prazo de entrega do item é obrigatório.");

        if (!StringUtils.hasText(categoryGuid))
            throw new IllegalArgumentException("Checklist a Categoria do item é obrigatório.");

    }

    public CheckListItemResponse addCheckListItem(CheckListItemRequest itemRequest) {

        this.validateCkeckListItem(itemRequest.getDescription(), itemRequest.getIsCompleted(),
                itemRequest.getDeadline(), itemRequest.getCategoryGuid());

        CheckListItemEntity listItemEntity = CheckListItemEntity.builder()
                .guid(UUID.randomUUID().toString())
                .description(itemRequest.getDescription())
                .isCompleted(itemRequest.getIsCompleted())
                .deadline(itemRequest.getDeadline())
                .category(categoryService.getCategoryEntityByGuid(itemRequest.getCategoryGuid()))
                .build();

        log.debug("Cadastrando novo CheckList [ CheckList = {} ]", listItemEntity);

        return mapper.checkListItemEntityToCheckListItemResponse(itemRepository.save(listItemEntity));
    }

    public CheckListItemResponse updateChecklistItem(CheckListItemUpdateRequest itemRequest) {
        validGUID(itemRequest.getCategoryGuid());
        CheckListItemEntity listItemEntity = this.itemRepository.findByGuid(itemRequest.getGuid())
                .orElseThrow(() -> new ResourceNotFoundException("Checklist não encontrado."));

        if (StringUtils.hasText(itemRequest.getDescription()))
            listItemEntity.setDescription(itemRequest.getDescription());
        if (itemRequest.getIsCompleted() != null)
            listItemEntity.setIsCompleted(itemRequest.getIsCompleted());
        if (itemRequest.getDeadline() != null)
            listItemEntity.setDeadline(itemRequest.getDeadline());
        if (StringUtils.hasText(itemRequest.getCategoryGuid()))
            listItemEntity.setCategory(categoryService.getCategoryEntityByGuid(itemRequest.getCategoryGuid()));

        log.debug("Atualizando CheckList [ CheckList = {} ]", listItemEntity);

        return mapper.checkListItemEntityToCheckListItemResponse(itemRepository.save(listItemEntity));
    }

    public List<CheckListItemResponse> findAllCheckList() {
        return mapper.listCheckListItemEntityToListCheckListItemResponse(itemRepository.findAll());
    }

    public void deleteChecklistItem(String guid) {

        validGUID(guid);
        CheckListItemEntity listItemEntity = this.itemRepository.findByGuid(guid)
                .orElseThrow(() -> new ResourceNotFoundException("Checklist não encontrado."));

        log.debug("Deletando CheckList [ guid = {} ] ", guid);
        this.itemRepository.delete(listItemEntity);

    }

    public CheckListItemResponse findChecklistItemByGuid(String guid) {

        validGUID(guid);
        return mapper.checkListItemEntityToCheckListItemResponse(this.itemRepository.findByGuid(guid)
                .orElseThrow(() -> new ResourceNotFoundException("Checklist Item não encontrado.")));

    }

    private void validGUID(String guid) {
        if (!StringUtils.hasText(guid))
            throw new IllegalArgumentException("GUID Checklist não pode ser Vazio ou Nulo.");
    }

    public void updateIsCompledStatus(String guid, boolean status) {
        validGUID(guid);
        CheckListItemEntity listItemEntity = this.itemRepository.findByGuid(guid)
                .orElseThrow(() -> new ResourceNotFoundException("Checklist não encontrado."));

        listItemEntity.setIsCompleted(status);

        log.debug("Atualizando status CheckList [ guid = {} ] - [ isCompleted = {} ] ", guid, status);
        this.itemRepository.save(listItemEntity);
    }
}
