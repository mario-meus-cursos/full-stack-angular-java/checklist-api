package com.estudo.checklistapi.services;

import com.estudo.checklistapi.exceptions.CatergoryException;
import com.estudo.checklistapi.exceptions.ResourceNotFoundException;
import com.estudo.checklistapi.mappers.CheckListMapper;
import com.estudo.checklistapi.models.entityes.CategoryEntity;
import com.estudo.checklistapi.models.entityes.CheckListItemEntity;
import com.estudo.checklistapi.models.requests.CategoryRequest;
import com.estudo.checklistapi.models.requests.CategoryUpdateRequest;
import com.estudo.checklistapi.models.responses.CategoryResponse;
import com.estudo.checklistapi.repositories.ICategoryRepository;
import com.estudo.checklistapi.repositories.ICheckListItemRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;


@Service
@Transactional
@Slf4j
public class CategoryService {

    private ICategoryRepository iCategoryRepository;
    private CheckListMapper mapper;
    private ICheckListItemRepository iCheckListItemRepository;

    public CategoryService(ICategoryRepository categoryRepository, CheckListMapper mapper,
                           ICheckListItemRepository iCheckListItemRepository) {
        this.iCategoryRepository = categoryRepository;
        this.mapper = mapper;
        this.iCheckListItemRepository = iCheckListItemRepository;
    }


    public CategoryResponse addCategory(CategoryRequest categoryRequest) {
        if (!StringUtils.hasText(categoryRequest.getName())) {
            throw new IllegalArgumentException("Nome da Categoria não pode ser Vazio ou Nulo.");
        }
        if (iCategoryRepository.findByName(categoryRequest.getName()).isPresent()) {
            throw new IllegalArgumentException("Categoria " + categoryRequest.getName() + " já existe");
        }

        CategoryEntity category = CategoryEntity.builder()
                .guid(UUID.randomUUID().toString())
                .name(categoryRequest.getName()).build();

        log.debug("Cadastrando Categoria [ name = {} ]", categoryRequest.getName());

        return mapper.categoryEntityToCategoryResponse(iCategoryRepository.save(category));
    }

    public CategoryResponse updateCategory(CategoryUpdateRequest updateRequest) {
        if (updateRequest.getGuid() == null || !StringUtils.hasText(updateRequest.getName())) {
            throw new IllegalArgumentException("Paramentros invalidos para Atualização da Categoria");
        }

        CategoryEntity category = this.iCategoryRepository.findByGuid(updateRequest.getGuid())
                .orElseThrow(() -> new ResourceNotFoundException("Categoria não encontrada."));

        category.setName(updateRequest.getName());

        log.debug("Atualizando categoria [ guid = {} ] - [ name = {} ]", updateRequest.getGuid(),
                updateRequest.getName());
        return mapper.categoryEntityToCategoryResponse(iCategoryRepository.save(category));
    }

    public void deleteCategory(String guid) {

        validGUID(guid);

        CategoryEntity category = this.iCategoryRepository.findByGuid(guid)
                .orElseThrow(() -> new ResourceNotFoundException("Categoria não encontrada."));

        List<CheckListItemEntity> checkLisItems = this.iCheckListItemRepository.findByCategoryGuid(guid);

        if (!CollectionUtils.isEmpty(checkLisItems))
            throw new CatergoryException("Categoria não pode ser Excluida! Catergoria em uso.");

        log.debug("Deletando categoria [ guid = {} ] ", guid);
        this.iCategoryRepository.delete(category);

    }
    public CategoryEntity getCategoryEntityByGuid(String guid) {

        validGUID(guid);
        return this.iCategoryRepository.findByGuid(guid)
                .orElseThrow(() -> new ResourceNotFoundException("Categoria não encontrada."));

    }

    public List<CategoryResponse> findAllCategory() {
        return mapper.lislCategoryEntityToListCategoryResponse(iCategoryRepository.findAll());
    }

    private void validGUID(String guid) {
        if (!StringUtils.hasText(guid))
            throw new IllegalArgumentException("GUID Categoria não pode ser Vazio ou Nulo.");
    }
}
