package com.estudo.checklistapi.models.requests;


import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@SuperBuilder
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
public class CheckListItemRequest {
    @NotBlank(message = "A Descrição do Checklist não pode ser nulo ou vazio.")
    private String description;
    @NotNull(message = "O status do Checklist não pode ser nulo ou vazio.")
    private Boolean isCompleted;
    @NotNull(message = "O Prazo para termino do Checklist não pode ser nulo ou vazio.")
    private LocalDate deadline;
    private String categoryGuid;
}
