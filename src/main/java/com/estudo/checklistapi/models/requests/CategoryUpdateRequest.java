package com.estudo.checklistapi.models.requests;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class CategoryUpdateRequest extends CategoryRequest{
    @NotBlank(message = "O GUID da categoria não pode ser nulo ou vazio.")
    private String guid;
}
