package com.estudo.checklistapi.models.entityes;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDate;

@SuperBuilder
@Entity(name = "checkListItem")
@Table(indexes = { @Index(name = "IDX_GUID_CK_IT" , columnList = "guid")})
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CheckListItemEntity extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ChecklistItemId;
    private String description;
    private Boolean isCompleted;
    private LocalDate deadline;
    @CreationTimestamp
    private LocalDate postDate;
    @ManyToOne
    private CategoryEntity category;
}
