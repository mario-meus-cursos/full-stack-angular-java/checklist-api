package com.estudo.checklistapi.models.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckListItemResponse {
    private String guid;
    private String description;
    private Boolean isCompleted;
    private LocalDate deadline;
    private LocalDate postDate;
    private CategoryResponse category;
}

