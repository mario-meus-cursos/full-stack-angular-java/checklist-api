package com.estudo.checklistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckListApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckListApiApplication.class, args);
	}

}
