package com.estudo.checklistapi.configs;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.context.annotation.Profile;

@Configuration
public class Config {

    @Bean
    public WebMvcConfigurer corsLocalConfig() {

        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("http://localhost:4200")
                        .allowedMethods("GET", "PUT", "OPTIONS", "POST", "DELETE", "PATCH")
                        .maxAge(900)
                        .allowedHeaders("Origin", "Content-Type", "X-Requested-With", "Accept", "Authorization");

            }
        };
    }

    @Profile("aws")
    @Bean
    public WebMvcConfigurer corsAwsConfig() {

        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("http://checklist-mario-spa.s3-website-us-east-1.amazonaws.com")
                        .allowedMethods("GET", "PUT", "OPTIONS", "POST", "DELETE", "PATCH")
                        .maxAge(900)
                        .allowedHeaders("Origin", "Content-Type", "X-Requested-With", "Accept", "Authorization");
            }
        };
    }

    @Bean
    public OpenAPI customOpemApi() {
        return new OpenAPI()
                .info(
                        new Info()
                                .title("Checklist API - Curso")
                                .description("Simples API de gerencialmente de checklist")
                                .contact(new Contact()
                                        .email("mario7lagoas@gmail.com")
                                        .name("Mario Sergio"))
                                .version("v1")
                                .license(
                                        new License()
                                                .name("Apache 2.0")
                                )

                );
    }
}
