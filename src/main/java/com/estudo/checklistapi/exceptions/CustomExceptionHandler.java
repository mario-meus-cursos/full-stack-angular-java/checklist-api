package com.estudo.checklistapi.exceptions;

import com.estudo.checklistapi.models.responses.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> resourceNotFound(ResourceNotFoundException ex) {

        Map<String, String> response = new HashMap<>();
        response.put("codigo", "404");
        response.put("mensagem", ex.getMessage());

        ErrorResponse errorResponse = ErrorResponse.builder()
                .status(HttpStatus.NOT_FOUND.toString())
                .erros(Collections.singletonList(response))
                .build();

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

    }
    @ExceptionHandler(CatergoryException.class)
    public ResponseEntity<ErrorResponse> resourceNotFound(CatergoryException ex) {

        Map<String, String> response = new HashMap<>();
        response.put("codigo", "404");
        response.put("mensagem", ex.getMessage());

        ErrorResponse errorResponse = ErrorResponse.builder()
                .status(HttpStatus.NOT_FOUND.toString())
                .erros(Collections.singletonList(response))
                .build();

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CheckListItemException.class)
    public ResponseEntity<ErrorResponse> resourceNotFound(CheckListItemException ex) {

        Map<String, String> response = new HashMap<>();
        response.put("codigo", "404");
        response.put("mensagem", ex.getMessage());

        ErrorResponse errorResponse = ErrorResponse.builder()
                .status(HttpStatus.NOT_FOUND.toString())
                .erros(Collections.singletonList(response))
                .build();

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoResourceFoundException.class)
    public ResponseEntity<ErrorResponse> oResourceFoundHandle(NoResourceFoundException ex) {

        Map<String, String> response = new HashMap<>();
        response.put("codigo", "ENDPOINT_NAO_ENCONTRADO");
        response.put("ensagem", ex.getBody().toString());

        ErrorResponse errorresponse = ErrorResponse.builder()
                .status(HttpStatus.NOT_FOUND.toString())
                .erros(Collections.singletonList(response))
                .build();

        return new ResponseEntity<>(errorresponse, HttpStatus.NOT_FOUND);
    }

}
