package com.estudo.checklistapi.exceptions;

public class CheckListItemException extends RuntimeException{

    public CheckListItemException(String mensagem){
        super(mensagem);
    }
}
