package com.estudo.checklistapi.repositories;

import com.estudo.checklistapi.models.entityes.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface ICategoryRepository extends JpaRepository<CategoryEntity, Long> {

    Optional<CategoryEntity> findByGuid(String guid);
    Optional<CategoryEntity> findByName(String name);

}
