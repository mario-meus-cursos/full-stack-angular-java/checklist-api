package com.estudo.checklistapi.repositories;

import com.estudo.checklistapi.models.entityes.CheckListItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ICheckListItemRepository extends JpaRepository<CheckListItemEntity, Long> {

    Optional<CheckListItemEntity> findByGuid(String guid);

    List<CheckListItemEntity> findByDescriptionAndIsCompleted(String description, Boolean isCompleted);

    List<CheckListItemEntity> findByCategoryGuid(String guid);
}
