package com.estudo.checklistapi.controlles;

import com.estudo.checklistapi.models.requests.CategoryRequest;
import com.estudo.checklistapi.models.requests.CategoryUpdateRequest;
import com.estudo.checklistapi.models.responses.CategoryResponse;
import com.estudo.checklistapi.services.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ApiResponse(responseCode = "200", description = "Categorias encontradas com sucesso.")
    @Operation(description = "Retorna uma lista de Categorias.")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CategoryResponse>> getAllCatergories() {
        return new ResponseEntity<>(categoryService.findAllCategory(), HttpStatus.OK);
    }

    @ApiResponse(responseCode = "201", description = "Cadastro de Categoria realizado com sucesso.")
    @Operation(description = "Cadastro de Categoria.")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryResponse> createNewCategory(@RequestBody @Valid CategoryRequest categoryRequest) {

        return new ResponseEntity<>(categoryService.addCategory(categoryRequest), HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Alteração de Categoria realizado com sucesso."),
            @ApiResponse(responseCode = "404", description = "Categoria não encontrado.")
    })
    @Operation(description = "Alteração de Categoria.")
    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryResponse> updateCategory(@RequestBody @Valid CategoryUpdateRequest updateRequest) {
        return new ResponseEntity<>(categoryService.updateCategory(updateRequest), HttpStatus.ACCEPTED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Exclusão de Categoria realizado com sucesso."),
            @ApiResponse(responseCode = "404", description = "Categoria não encontrado.")
    })
    @Operation(description = "Exclusão de Categoria pelo GUID.")
    @DeleteMapping(value = "{guid}")
    public ResponseEntity<Void> deleteCategory(@PathVariable("guid") String guid) {
        categoryService.deleteCategory(guid);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
