package com.estudo.checklistapi.controlles;

import com.estudo.checklistapi.models.requests.CheckListItemRequest;
import com.estudo.checklistapi.models.requests.CheckListItemUpdateRequest;
import com.estudo.checklistapi.models.requests.UpdateStatusRequest;
import com.estudo.checklistapi.models.responses.CheckListItemResponse;
import com.estudo.checklistapi.services.CheckListItemService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/checklist-items")
public class CheckListItemController {

    private CheckListItemService itemService;

    public CheckListItemController(CheckListItemService itemService) {
        this.itemService = itemService;
    }

    @ApiResponse(responseCode = "200", description = "Lista de Checklist realizado com sucesso.")
    @Operation(description = "Retorna lista de Checklist.")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CheckListItemResponse>> getAllCheckList() {

        return new ResponseEntity<>(itemService.findAllCheckList(), HttpStatus.OK);
    }

    @ApiResponse(responseCode = "201", description = "Cadastro de Checklist realizado com sucesso.")
    @Operation(description = "Cadastro de Checklist.")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CheckListItemResponse> createNewChecklistItem(@RequestBody @Valid CheckListItemRequest itemRequest) {

        return new ResponseEntity<>(itemService.addCheckListItem(itemRequest), HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Alteração de Checklist realizado com sucesso."),
            @ApiResponse(responseCode = "404", description = "Checklist não encontrado.")
    })
    @Operation(description = "Alteração de Checklist.")
    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CheckListItemResponse> updateChecklistItem(@RequestBody @Valid CheckListItemUpdateRequest itemUpdateRequest) {
        return new ResponseEntity<>(itemService.updateChecklistItem(itemUpdateRequest), HttpStatus.ACCEPTED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Exclusão de Checklist realizado com sucesso."),
            @ApiResponse(responseCode = "404", description = "Checklist não encontrado.")
    })
    @Operation(description = "Exclusão de Checklist pelo GUID.")
    @DeleteMapping(value = "{guid}")
    public ResponseEntity<Void> deleteChecklistItem(@PathVariable("guid") String guid) {
        this.itemService.deleteChecklistItem(guid);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Alteração Do Status do Checklist realizado com sucesso."),
            @ApiResponse(responseCode = "404", description = "Checklist não encontrado.")
    })
    @PatchMapping(value = "{guid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateCompledStatus(@PathVariable("guid") String guid,
                                                    @RequestBody UpdateStatusRequest status){

        this.itemService.updateIsCompledStatus(guid, status.isCompleted());
        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }
}
