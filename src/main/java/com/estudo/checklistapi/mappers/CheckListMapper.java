package com.estudo.checklistapi.mappers;

import com.estudo.checklistapi.models.entityes.CategoryEntity;
import com.estudo.checklistapi.models.entityes.CheckListItemEntity;
import com.estudo.checklistapi.models.responses.CategoryResponse;
import com.estudo.checklistapi.models.responses.CheckListItemResponse;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CheckListMapper {

    List<CategoryResponse> lislCategoryEntityToListCategoryResponse(List<CategoryEntity> categories);
    CategoryResponse categoryEntityToCategoryResponse(CategoryEntity category);
    List<CheckListItemResponse> listCheckListItemEntityToListCheckListItemResponse(List<CheckListItemEntity> listItemEntities);
    CheckListItemResponse checkListItemEntityToCheckListItemResponse(CheckListItemEntity listItemEntity);
}
